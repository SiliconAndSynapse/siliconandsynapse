const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
    mode: "development",
    entry: {
        app: path.resolve(__dirname, "./src/index.js"),
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].[hash:8].js"
    },
    devServer: {
        port: 7870,
        index: "index.html",
        hotOnly: true,
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
            },
            {
                test: /\.(css|less)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'less-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                },
            },
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx"],
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@config': path.resolve(__dirname, 'src/config'),
            '@constants': path.resolve(__dirname, 'src/constants'),
            '@modules': path.resolve(__dirname, 'src/modules'),
            '@redux': path.resolve(__dirname, 'src/rootReducer/reducers'),
            '@sagas': path.resolve(__dirname, 'src/sagas'),
            '@selectors': path.resolve(__dirname, 'src/selectors'),
            '@styles': path.resolve(__dirname, 'src/styles'),
            '@utils': path.resolve(__dirname, 'src/utils'),
        }

    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./public/index.html",
            chunks: ["app"]
        }),
        new MiniCssExtractPlugin({
            filename: "[name].[hash:8].css",
        }),
    ],
    devtool: "source-map",
};
