import actionTypes from "../../constants/actionTypes";

export const togglePage = () => ({type: actionTypes.TOGGLE_PAGE});
export const togglePageSaga = payload => ({ type: actionTypes.TOGGLE_MODE_ASYNC, payload });

