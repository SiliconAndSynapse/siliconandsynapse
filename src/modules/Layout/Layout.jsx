import React from 'react';

export default class Layout extends React.Component {

  getRenderAuth = () => (
    <React.Fragment>
      <h1>Auth</h1>
    </React.Fragment>
  );

  getRenderRegistrants = () => (
    <React.Fragment>
      <h1>Registrants</h1>
    </React.Fragment>
  );

  changeIsPageToggleSaga = () => {
    const { togglePageSaga, togglePage } = this.props;
    togglePageSaga();
    // togglePage()
  };

  render() {
    const { isPageToggle } = this.props;
    return (
      <div>
        {isPageToggle ? this.getRenderAuth() : this.getRenderRegistrants()}
        <button onClick={this.changeIsPageToggleSaga} type='button'>Click</button>
      </div>
    );
  }
}
