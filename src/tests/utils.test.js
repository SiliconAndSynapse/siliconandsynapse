import {sumMax} from '../utils/utils';

describe('Sum max', () => {
  it('max 5 and 7', () => {
    const action = sumMax(5, 7);
    const expect = 7;
    assert.equal(action, expect, '7 bigest then 5');
  });
});