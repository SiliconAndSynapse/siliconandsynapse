import {all, fork} from 'redux-saga/effects';
import {watchPageState} from './sagas';

const sagas = [
  watchPageState,
];

export default function* rootSaga() {
  yield all(sagas.map(fork));
};