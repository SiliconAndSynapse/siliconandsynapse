import {takeEvery, put } from 'redux-saga/effects';
import actionTypes from "../constants/actionTypes";

export function* watchPageState() {
  yield takeEvery(actionTypes.TOGGLE_MODE_ASYNC, changePageSaga);
}

function* changePageSaga(action) {
  yield put({type: actionTypes.TOGGLE_PAGE, payload: action.payload});
  console.log('work saga');
}