import {commonReducer} from "./reducers/commonReducer";
import {combineReducers} from "redux";

export default combineReducers({
    common: commonReducer,
});