const initialState = {
  data: {},
  IsPageToggle: false,
};

export const commonReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TOGGLE_PAGE": return {
      ...state,
      IsPageToggle: !state.IsPageToggle
    };
    case "ADD_DATA": return {
      ...state,
      data: action.payload
    };
    default: return state
  }
};