import React from "react";
import ReactDOM from "react-dom";
import Layout from "./modules/Layout/index";
import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux";
import rootReducer from "./rootReducer/rootReducer";
import rootSaga from './sagas/rootSaga.js';
import createSagaMiddleware from 'redux-saga';
import "./styles/index.less";
//
const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
window.store = store;
sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}><Layout/></Provider>,
  document.getElementById("root")
);


