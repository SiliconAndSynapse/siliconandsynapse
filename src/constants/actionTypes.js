import keyMirror from "key-mirror";

export default keyMirror({
    TOGGLE_PAGE: null,
    ADD_DATA: null,
    TOGGLE_MODE_ASYNC:null,
});